from django.db import models

# Create your models here.


class Device(models.Model):
    type = models.CharField(max_length=100, blank=False)
    price = models.IntegerField()
    choices = (
        ('AVAILABLE', 'Item ready to be purchased'),
        ('SOLD', 'Item Sold'),
        ('RESTOCKING', 'Item restocking in few days')
    )
    status = models.CharField(max_length=10, choices=choices, default="SOLD")
    issues = models.CharField(max_length=100, default="No issues")

    class Meta:
        abstract = True

    def __str__(self):
        return 'Type: {0} Price: {1}'.format(self.type, self.price)


class Laptop(Device):
    pass


class Desktop(Device):
    pass


class Mobile(Device):
    pass


class RestTest(models.Model):
    symbol_number = models.CharField(max_length=8)
    exam_center = models.CharField(max_length=30)
    address = models.CharField(max_length=30)
    password = models.CharField(max_length=10, blank=True, null=True)

    def __str__(self):
        return self.symbol_number


class RestChild(models.Model):
    user_name = models.CharField(max_length=25)
    user = models.ForeignKey(RestTest, on_delete=models.CASCADE)
