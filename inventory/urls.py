from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.routers import DefaultRouter

# from .views import index, display_laptops,
from inventory import views

default_router = DefaultRouter()
default_router.register('rest/test', views.UserView, basename=None)

default_router.register('rest/child/test', views.RestChildView, basename='rest_child_test')

urlpatterns = [
                  path('', views.index, name='index'),

                  path('display_laptop', views.display_laptop, name='display_laptop'),
                  path('display_desktop', views.display_desktop, name='display_desktop'),
                  path('display_mobile', views.display_mobile, name='display_mobile'),

                  path('add_laptop', views.add_laptop, name='add_laptop'),
                  path('add_desktop', views.add_desktop, name='add_desktop'),
                  path('add_mobile', views.add_mobile, name='add_mobile'),

                  path(r'^edit_laptop/(?P<pk>\d+)$', views.edit_laptop, name='edit_laptop'),
                  path(r'^edit_desktop/(?P<pk>\d+)$', views.edit_desktop, name='edit_desktop'),
                  path(r'^edit_mobile/(?P<pk>\d+)$', views.edit_mobile, name='edit_mobile'),

                  path(r'^delete_laptop/(?P<pk>\t+)$', views.delete_laptop, name="delete_laptop"),
                  path(r'^delete_desktop/(?P<pk>\t+)$', views.delete_desktop, name="delete_desktop"),
                  path(r'^delete_mobile/(?P<pk>\t+)$', views.delete_mobile, name="delete_mobile"),

                  # path('rest/test/', views.RestTestView.as_view(), name='rest_test'),

                  # path('rest/test/', views.UserView.as_view({'get': 'list', 'post': 'create'}), name="rest_test"),
                  path('login/', obtain_auth_token, name='login'),

              ] + default_router.urls
