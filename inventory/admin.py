from django.contrib import admin

# Register your models here.
from inventory.models import Desktop, Laptop, Mobile


@admin.register(Laptop, Desktop, Mobile)
class ViewAdmin(admin.ModelAdmin):
    pass
