from django.shortcuts import render, redirect, get_object_or_404
# from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

# from rest_framework.views import APIView
# from rest_framework.response import Response
# from rest_framework.views import status
#
from inventory.forms import LaptopForm, DesktopForm, MobileForm
from inventory.models import Laptop, Desktop, Mobile

from .models import RestTest, RestChild
from .permission import IsAdminUser, IsNormalUser
from .serializers import UserSerializer, RestChildSerializer


# from .serializers import UserSerializer
# from django.contrib.auth.hashers import make_password


def index(request):
    return render(request, 'inventory/index.html')


def display_laptop(request):
    items = Laptop.objects.all()
    context = {
        'items': items,
        'header': 'Laptop',
    }
    return render(request, "inventory/index.html", context=context)


def display_desktop(request):
    items = Desktop.objects.all()
    context = {
        'items': items,
        'header': 'Desktop',
    }
    return render(request, "inventory/index.html", context=context)


def display_mobile(request):
    items = Mobile.objects.all()
    context = {
        'items': items,
        'header': 'Mobile',
    }
    return render(request, "inventory/index.html", context=context)


# def add_laptop(request):
#     if request.method == 'POST':
#         form = LaptopForm(request.POST)
#
#         if form.is_valid():
#             form.save()
#             return redirect("index")
#
#     else:
#         form = LaptopForm()
#         return render(request, "add_new.html", context={'form': form})
#
#
# def add_desktop(request):
#     if request.method == 'POST':
#         form = DesktopForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect("index")
#     else:
#         form = DesktopForm()
#         return render(request,'add_new.html', context={'form': form})
#
#
# def add_mobile(request):
#     if request.method == "POST":
#         form = MobileForm(request.POST)
#
#         if form.is_valid():
#             form.save()
#             return redirect("index")
#     else:
#         form = MobileForm()
#         return render(request, 'add_new.html', context={'form': form})


def add_device(request, cls):
    if request.method == 'POST':
        form = cls(request.POST)

        if form.is_valid():
            form.save()
            return redirect("index")

    else:
        form = cls()
        return render(request, "inventory/add_new.html", context={'form': form})


def add_laptop(request):
    return add_device(request, LaptopForm)


def add_desktop(request):
    return add_device(request, DesktopForm)


def add_mobile(request):
    return add_device(request, MobileForm)


# def edit_laptop(request, pk):
#     item = get_object_or_404(Laptop, pk=pk)
#     if request.method == 'POST':
#         form = LaptopForm(request.POST, instance=item)
#         if form.is_valid():
#             form.save()
#             return redirect("index")
#     else:
#         form = LaptopForm(instance=item)
#         return render(request, 'edit_item.html', {'form': form})


def edit_device(request, pk, model, cls):
    item = get_object_or_404(model, pk=pk)
    if request.method == 'POST':
        form = cls(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("index")
    else:
        form = cls(instance=item)
        return render(request, 'inventory/edit_device.html', {'form': form})


def edit_laptop(request, pk):
    return edit_device(request, pk, Laptop, LaptopForm)


def edit_desktop(request, pk):
    return edit_device(request, pk, Desktop, DesktopForm)


def edit_mobile(request, pk):
    return edit_device(request, pk, Mobile, MobileForm)


def delete_laptop(request, pk):
    Laptop.objects.filter(id=pk).delete()
    items = Laptop.objects.all()
    context = {
        'items': items
    }
    return render(request, 'inventory/index.html', context)


def delete_desktop(request, pk):
    Desktop.objects.filter(id=pk).delete()
    items = Desktop.objects.all()
    context = {
        'items': items
    }
    return render(request, 'inventory/index.html', context)


def delete_mobile(request, pk):
    Mobile.objects.filter(id=pk).delete()
    items = Mobile.objects.all()
    context = {
        'items': items
    }
    return render(request, 'inventory/index.html', context)


# class RestTestView(APIView):
#     def get(self, request):
#         # name = {'name': 'nabin', 'roll No.': '09', 'college': 'ASMT'}
#
#         # syn_no = request.GET.get('symbol_number'),
#         # exm_center = request.GET.get('exam_center'),
#         # add = request.GET.get('address')
#         # RestTest.objects.create(symbol_number=syn_no, exam_center=exm_center, address=add)
#         # print(RestTest.objects.get(pk=1))
#
#         # serial = RestTest.objects.get(id=1)
#         serial = RestTest.objects.all()
#         serial1 = UserSerializer(serial, many=True)
#
#         # database_data = []
#         # for data in RestTest.objects.all():
#         #     database_data.append({'sym_no': data.symbol_number,
#         #                           'exm_cnt': data.exam_center,
#         #                           'addr': data.address,
#         #                           })
#
#         return Response(serial1.data, status=status.HTTP_200_OK)
#
#     def post(self, request):
#         # syn_no = request.data.get('symbol_number'),
#         # exm_center = request.data.get('exam_center'),
#         # add = request.data.get('address')
#         # print(syn_no, add, exm_center)
#         # RestTest.objects.create(symbol_number=syn_no, exam_center=exm_center, address=add)
#
#         password = request.data.pop('password')
#         print(password)
#         serial = UserSerializer(data=request.data)
#         if serial.is_valid():
#             # RestTest.objects.create(symbol_number= syn_no, exam_center=exm_center, address=add)
#             validated_data = serial.validated_data
#             data = RestTest.objects.create(**validated_data)
#             data.password = make_password(password=password, salt=None, hasher='default')
#             data.save()
#             print("data saved")
#             # RestTest.objects.create(**serial.validated_data)
#         print("not validated")
#         return Response({'msg': 'successful'}, status=status.HTTP_200_OK)


class UserView(ModelViewSet):
    # authentication_classes = [IsAuthenticated]
    queryset = RestTest.objects.all()
    serializer_class = UserSerializer


class RestChildView(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = RestChild.objects.all()
    serializer_class = RestChildSerializer

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_queryset(), many=True)
        r = {
            "message": "Success Message",
            "data": serializer.data
        }
        return Response(r)

    def get_permissions(self):
        get_list = []
        if self.action == 'list':
            get_list.append(IsAuthenticated)
        elif self.action == 'create':
            # AdminUser()
            get_list.append(IsAdminUser)
        return [permission() for permission in get_list]
