# from rest_framework import serializers
# from rest_framework.views import APIView
# from rest_framework.response import Response
# from rest_framework.views import status


# class UserSerializer(serializers.Serializer):
#     symbol_number = serializers.CharField(max_length=8)
#     exam_center = serializers.CharField(max_length=30)
#     address = serializers.CharField(max_length=30)
#     # password = serializers.CharField(max_length=100)


from rest_framework.serializers import ModelSerializer

from .models import RestTest, RestChild


class UserSerializer(ModelSerializer):
    # def to_representation(self, instance):
    #     # r = super().to_representation(instance)
    #     return instance.__class__.__name__

    class Meta:
        model = RestTest
        fields = '__all__'

    # def to_representation(self, instance):
    #     request = super().to_representation(instance)


class RestChildSerializer(ModelSerializer):
    # user = UserSerializer()

    class Meta:
        model = RestChild
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        obj = UserSerializer(instance.user)
        response['user'] = obj.data
        return response

