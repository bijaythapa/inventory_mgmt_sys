from django.urls import path
from . import views

urlpatterns = [
    path("register/", views.register, name="register"),
    path("register/user_register", views.user_register, name="user_register"),
    path("login/", views.login, name="login"),
    path("login/user_login", views.user_login, name="user_login"),
    path("logout/", views.logout, name="logout"),
    path('', views.dashboard, name="dashboard"),
]
